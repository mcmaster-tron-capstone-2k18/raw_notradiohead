#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>

#define DISABLE_INVERT_IQ_ON_RX
#define LED_BUILTIN (13)

//#if !defined(DISABLE_INVERT_IQ_ON_RX)
//#error This example requires DISABLE_INVERT_IQ_ON_RX to be set. Update config.h in the lmic library to set it.
//#endif

// How often to send a packet. Note that this sketch bypasses the normal
// LMIC duty cycle limiting, so when you change anything in this sketch
// (payload length, frequency, spreading factor), be sure to check if
// this interval should not also be increased.
// See this spreadsheet for an easy airtime and duty cycle calculator:
// https://docs.google.com/spreadsheets/d/1voGAtQAjC1qBmaVuP1ApNKs1ekgUjavHuVQIXyYSvNc 
//#define TX_INTERVAL 2000

// Pin mapping
const lmic_pinmap lmic_pins = {
    6,   //nss
    LMIC_UNUSED_PIN,    //rxtx
    5,  //rst
    {2, 3, 4},  //dio
};


//void onEvent (ev_t ev) {
//}

//osjob_t txjob;
//osjob_t timeoutjob;
//static void tx_func (osjob_t* job);

//static void rxtimeout_func(osjob_t *job) {
//  digitalWrite(LED_BUILTIN, LOW); // off
//}

//static void rx_func (osjob_t* job) {
//  // Blink once to confirm reception and then keep the led on
//  digitalWrite(LED_BUILTIN, LOW); // off
//  delay(10);
//  digitalWrite(LED_BUILTIN, HIGH); // on
//
//  // Timeout RX (i.e. update led status) after 3 periods without RX
//  os_setTimedCallback(&timeoutjob, os_getTime() + ms2osticks(3*TX_INTERVAL), rxtimeout_func);
//
//  // Reschedule TX so that it should not collide with the other side's
//  // next TX
//  os_setTimedCallback(&txjob, os_getTime() + ms2osticks(TX_INTERVAL/2), tx_func);
//
//  Serial.print("Got ");
//  Serial.print(LMIC.dataLen);
//  Serial.println(" bytes");
//  Serial.write(LMIC.frame, LMIC.dataLen);
//  Serial.println();
//
//  // Restart RX
//  rx(rx_func);
//}

//static void txdone_func (osjob_t* job) {
//  rx(rx_func);
//}

// log text to USART and toggle LED
//static void tx_func (osjob_t* job) {
//  // say hello
//  tx("Hello, world!", txdone_func);
//  // reschedule job every TX_INTERVAL (plus a bit of random to prevent
//  // systematic collisions), unless packets are received, then rx_func
//  // will reschedule at half this time.
//  os_setTimedCallback(job, os_getTime() + ms2osticks(TX_INTERVAL + random(500)), tx_func);
//}







void setup() {
  Serial.begin(115200);
  Serial.println("Starting");

//  pinMode(LED_BUILTIN, OUTPUT);

  // initialize runtime env
//  os_init();

  // Set up these settings once, and use them for both TX and RX
#define CFG_us915
  LMIC.freq = 902300000;

  LMIC.txpow = 27;  // Maximum TX power
  LMIC.datarate = DR_SF9;  // Use a medium spread factor
  LMIC.rps = updr2rps(LMIC.datarate);  // This sets CR 4/5, BW125 (except for DR_SF7B, which uses BW250)

  Serial.println("Started");
  Serial.flush();

  // setup initial job
//  os_setCallback(&txjob, tx_func);
}

void tx(const char *str) {
  os_radio(RADIO_RST); // Stop RX first
  delay(1); // Wait a bit, without this os_radio below asserts, apparently because the state hasn't changed yet
  LMIC.dataLen = 0;
  while (*str) {
    LMIC.frame[LMIC.dataLen++] = *str++;
  }
  os_radio(RADIO_TX);

  Serial.println("TX");
}

void rx() {
  LMIC.rxtime = os_getTime(); // RX _now_
  LMIC.rxsyms = 50;
  os_radio(RADIO_RX);
  Serial.println("RX");
}

void loop() {
  tx("Hello, world!");
  delay(1000);
  rx();
  Serial.print("Got ");
  Serial.print(LMIC.dataLen);
  Serial.println(" bytes");
  Serial.write(LMIC.frame, LMIC.dataLen);
  Serial.println();
}

ENERGIA_LOCATION ?= /home/cameron/tools/energia-1.8.7E21
ENERGIA_LIBS ?= ./libraries
TARGET_ARCH = MSP-EXP430FR4133LP
TARGET = raw_notradiohead.ino

BUILD_PATH = $(realpath .)/build
BUILD_CACHE = $(realpath .)/build-cache

FLAGS = -ide-version=10807
FLAGS += -hardware $(ENERGIA_LOCATION)/hardware
FLAGS += -tools $(ENERGIA_LOCATION)/tools-builder
FLAGS += -tools $(ENERGIA_LOCATION)/hardware/tools/msp430
FLAGS += -built-in-libraries $(ENERGIA_LOCATION)/libraries
FLAGS += -libraries $(ENERGIA_LIBS)
FLAGS += -fqbn=energia:msp430:$(TARGET_ARCH)
FLAGS += -prefs=runtime.tools.msp430-gcc.path=$(ENERGIA_LOCATION)/hardware/tools/msp430
FLAGS += -prefs=runtime.tools.msp430-gcc-4.6.6.path=$(ENERGIA_LOCATION)/hardware/tools/msp430
FLAGS += -prefs=runtime.tools.dslite.path=$(ENERGIA_LOCATION)/hardware/tools/DSLite
FLAGS += -prefs=runtime.tools.dslite-8.2.0.1400.path=$(ENERGIA_LOCATION)/hardware/tools/DSLite
FLAGS += -prefs=runtime.tools.mspdebug.path=$(ENERGIA_LOCATION)/hardware/tools/mspdebug
FLAGS += -prefs=runtime.tools.mspdebug-0.24.path=$(ENERGIA_LOCATION)/hardware/tools/mspdebug
FLAGS += -logger=machine -verbose -warnings=all -prefs=build.warn_data_percentage=75
FLAGS += -build-path $(BUILD_PATH)
FLAGS += -build-cache $(BUILD_CACHE)

.PHONY: build upload clean

build:
	$(shell mkdir -p build build-cache)
	$(ENERGIA_LOCATION)/arduino-builder -compile $(FLAGS) $(TARGET)

#
upload: build
#	$(ENERGIA_LOCATION)/hardware/tools/DSLite/DebugServer/bin/DSLite load -c $(ENERGIA_LOCATION)/hardware/tools/DSLite/$(TARGET_ARCH).ccxml -f $(BUILD_PATH)/$(TARGET).elf
	$(ENERGIA_LOCATION)/hardware/tools/mspdebug/mspdebug rf2500 --force-reset "prog $(BUILD_PATH)/$(TARGET).hex"

clean:
	rm -r build build-cache
